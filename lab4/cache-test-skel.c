/*
Coursera HW/SW Interface
Lab 4 - Mystery Caches

Mystery Cache Geometries (for you to keep notes):
mystery0:
    block size = 64 B
    cache size = 4096 B
    associativity = 32
mystery1:
    block size = 8 B
    cache size = 8192 B
    associativity = 8
mystery2:
    block size = 32 B
    cache size = 32768 B
    associativity = 2
mystery3:
    block size = 16 B
    cache size = 4096 B
    associativity = 1
*/

#include <stdlib.h>
#include <stdio.h>

#include "mystery-cache.h"

/*
 * NOTE: When using access_cache() you do not need to provide a "real"
 * memory addresses. You can use any convenient integer value as a
 * memory address, you should not be able to cause a segmentation
 * fault by providing a memory address out of your programs address
 * space as the argument to access_cache.
 */

/*
   Returns the size (in B) of each block in the cache.
*/
int get_block_size(void) {
  /* YOUR CODE GOES HERE */
  int i=0;
  int a=0;
  for (i=0; i<128; i++) {
     if (access_cache(i)) {
         //printf("%i:HIT ", i);
     }
     else {/*printf("%i:MISS ", i);*/ a++;}
  }
  return 128/a;
}

/*
   Returns the size (in B) of the cache.
*/
int get_cache_size(int size) {
  /* YOUR CODE GOES HERE */
  int i=0;
  int j=0;
  int cache_size=4096;
  flush_cache();
  
  for(j=0; j<129; j++)
  {
    //printf("0:hit:%d\n", access_cache(0));
    access_cache(0);
    for(i=0; i<cache_size*j; i+=size) {
      //printf("%d:hit:%d\n", i, access_cache(i));
      access_cache(i);
    }
    if(!access_cache(0)) {return i-cache_size;}
  }
  return -1;
}

/*
   Returns the associativity of the cache.
*/
int get_cache_assoc(int size) {
  /* YOUR CODE GOES HERE */ 
  int i = 0;
  int j = 0;
  flush_cache();
  access_cache(0);
  for(j=0; j < size; j++) {
    access_cache(0);
    for (i = 1; i < j; i++) {
      access_cache(i*size);
    }
    if (!access_cache(0)) return i-1;
  }

  return -1;
}

//// DO NOT CHANGE ANYTHING BELOW THIS POINT
int main(void) {
  int size;
  int assoc;
  int block_size;

  /* The cache needs to be initialized, but the parameters will be
     ignored by the mystery caches, as they are hard coded.  You can
     test your geometry paramter discovery routines by calling
     cache_init() w/ your own size and block size values. */
  cache_init(0,0);

  block_size=get_block_size();
  size=get_cache_size(block_size);
  assoc=get_cache_assoc(size);

  printf("Cache block size: %d bytes\n", block_size);
  printf("Cache size: %d bytes\n", size);
  printf("Cache associativity: %d\n", assoc);

  return EXIT_SUCCESS;
}
